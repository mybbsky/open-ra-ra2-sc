# OpenRA RA2SC模组

[![Build Status](https://travis-ci.org/OpenRA/ra2.svg?branch=master)](https://travis-ci.org/OpenRA/ra2)

查阅[wiki](https://gitee.com/zgrsgr/open-ra-ra2-sc/wikis/)有关如何安装和使用此文件的说明

[![Bountysource](https://api.bountysource.com/badge/tracker?tracker_id=27677844)](https://www.bountysource.com/teams/openra/issues?tracker_ids=27677844)

EA尚未认可也不支持此产品。

ps：本汉化版采用OpenRA汉化组的汉化版引擎（prep-2003-sc），地址：https://gitee.com/CastleJing/OpenRA 。